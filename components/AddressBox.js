import { BsTrash } from "react-icons/bs";
import { useState } from "react";
import axios from "axios";
import Cookies from "universal-cookie";
import LoadingModule from "./LoadingModule";
import {
  Form,
  Input,
  TextArea,
  Button,
  Select,
  Modal,
} from "semantic-ui-react";

export default function AddressBox({
  address,
  userInfo,
  styles,
  deleteAddress,
  loadingModule,
  setLoadingModule,
}) {
  const cookies = new Cookies();
  const genderOptions = [
    { key: "m", text: "Male", value: "male" },
    { key: "f", text: "Female", value: "female" },
    { key: "o", text: "Other", value: "other" },
  ];
  const [openUpdate, setOpenUpdate] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [addressTitle, setAddressTitle] = useState(address.title);
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [addressDetail, setAddressDetail] = useState(address.detail);
  const [phone, setPhone] = useState(address.phone_number);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorMessageColor, setErrorMessageColor] = useState(true);

  const updateAddress = () => {
    let addressObject = {
      title: addressTitle,
      detail: addressDetail,
      city: 37,
      district: 1,
      phone_number: phone,
    };
    setOpenUpdate(true);
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/address/create`,
        addressObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setErrorMessage(res.data.message);
        setErrorMessageColor(true);
        setTimeout(() => {
          setOpenUpdate(false);
        }, 500);
      })
      .catch((err) => {
        setErrorMessage(err.response.data.message);
        setErrorMessageColor(false);
        setOpenUpdate(false);
      });
  };

  return (
    <div className={styles.addressWrapper}>
      <h3>{address.title}</h3>
      <div className={styles.addressTextContainer}>
        <span>{`${userInfo.name} ${userInfo.surname}`}</span>
        <span>{address.phone_number}</span>
        <span>{address.detail}</span>
        <span>{`${address.city_name}/${address.district_name}`}</span>
      </div>
      <div className={styles.addressButtons}>
        <button onClick={() => deleteAddress(address.address_id)}>
          <BsTrash /> Sil
        </button>
        <LoadingModule
          setLoadingModule={setLoadingModule}
          loadingModule={loadingModule}
        />

        {/* <Modal
          onClose={() => setOpenUpdate(false)}
          onOpen={() => setOpenUpdate(true)}
          open={openUpdate}
          trigger={<button>Adresi Düzenle</button>}
        >
          <Form
            className={styles.updateAddressModal}
            onSubmit={() => console.log("asd")}
          >
            {errorMessage ? (
              <p
                style={
                  errorMessageColor
                    ? { backgroundColor: "#e0eede" }
                    : { backgroundColor: "#eedede" }
                }
              >
                {errorMessage}
              </p>
            ) : null}
            <Form.Field
              id="form-input-control-first-name"
              control={Input}
              label="Adres Başlığı"
              placeholder="Ev Adresim, Ofis Adresim"
              required
              value={addressTitle}
              onChange={(e, { value }) => setAddressTitle(value)}
            />
            <Form.Group widths="equal">
              <Form.Field
                control={Select}
                options={genderOptions}
                label="İl"
                required
                placeholder="Seçiniz"
                search
                searchInput={{ id: "form-select-control-gender" }}
                onChange={(e, { value }) => setCity(value)}
              />
              <Form.Field
                control={Select}
                options={genderOptions}
                label="İlçe"
                required
                placeholder="Seçiniz"
                search
                searchInput={{ id: "form-select-control-gender" }}
                onChange={(e, { value }) => setDistrict(value)}
              />
            </Form.Group>

            <Form.Field
              id="form-textarea-control-opinion"
              control={TextArea}
              label="Adres Detayı"
              placeholder="Gül sokak, Beyler Sokak, no:60, daire:6"
              required
              value={addressDetail}
              onChange={(e, { value }) => setAddressDetail(value)}
            />
            <Form.Field
              id="form-input-control-error-email"
              control={Input}
              type="number"
              label="Telefon Numarası"
              placeholder="0533 336 6654"
              required
              value={phone}
              onChange={(e, { value }) => setPhone(value)}
            />
            <Form.Field
              id="form-button-control-public"
              control={Button}
              content="Güncelle"
              type="submit"
            />
          </Form>
        </Modal> */}
      </div>
    </div>
  );
}
