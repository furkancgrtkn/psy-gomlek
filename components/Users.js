import React from "react";
import styles from "../styles/Users.module.css";
export default function Users({ users }) {
  return (
    <div className={styles.usersContainer}>
      {users.map((user, index) => (
        <div key={index} className={styles.usersCard}>
          <span>
            <b>
              {user.name} {user.surname}
            </b>
          </span>
          <span>
            <b>Email</b> - {user.email}
          </span>
          <span>
            <b>Telefon</b> - {user.phone_number}
          </span>
        </div>
      ))}
    </div>
  );
}
