import styles from "../styles/MyAccountInfo.module.css";
import AddressBox from "./AddressBox";
import { BsTrash } from "react-icons/bs";
import { useState, useEffect } from "react";
import axios from "axios";
import Cookies from "universal-cookie";
import LoadingModule from "./LoadingModule";

import {
  Form,
  Input,
  TextArea,
  Button,
  Select,
  Modal,
} from "semantic-ui-react";

export default function MyAccountInfo({ userInfo }) {
  const cookies = new Cookies();
  const genderOptions = [
    { key: "m", text: "Male", value: "male" },
    { key: "f", text: "Female", value: "female" },
    { key: "o", text: "Other", value: "other" },
  ];
  const [openUpdate, setOpenUpdate] = useState(false);
  const [loadingModule, setLoadingModule] = useState(false);
  const [openNew, setOpenNew] = useState(false);
  const [addressTitle, setAddressTitle] = useState("");
  const [city, setCity] = useState("");
  const [district, setDistrict] = useState("");
  const [addressDetail, setAddressDetail] = useState("");
  const [phone, setPhone] = useState("");
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorMessageColor, setErrorMessageColor] = useState(true);
  const [addresses, setAddresses] = useState([]);
  const [cities, setCities] = useState([]);
  const [districts, setDistricts] = useState([]);

  const addAddress = () => {
    let addressObject = {
      title: addressTitle,
      detail: addressDetail,
      city: city,
      district: district,
      phone_number: phone,
    };
    setLoadingModule(true);
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/address/create`,
        addressObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setErrorMessage(res.data.message);
        setErrorMessageColor(true);
        axios
          .get(process.env.REACT_APP_CLIENT_API_URL + `/address`, {
            headers: {
              Authorization: `Bearer ${cookies.get("psyJWT")}`,
            },
          })
          .then((res) => {
            setAddresses(res.data.data);
          })
          .then((res) => {
            setOpenNew(false);
            setLoadingModule(false);
            setErrorMessage(null);
          });
      })
      .catch((err) => {
        setErrorMessage(err.response.data.message);
        setErrorMessageColor(false);
        setErrorMessage(null);
        setLoadingModule(false);
      });
  };

  const deleteAddress = (id) => {
    let addressObject = {
      id: id,
    };
    setLoadingModule(true);
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/address/delete`,
        addressObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        // setErrorMessage(res.data.message);
        axios
          .get(process.env.REACT_APP_CLIENT_API_URL + `/address`, {
            headers: {
              Authorization: `Bearer ${cookies.get("psyJWT")}`,
            },
          })
          .then((res) => {
            setAddresses(res.data.data);
          });
        setTimeout(() => {
          setLoadingModule(false);
        }, 500);
      })
      .catch((err) => {
        setLoadingModule(false);
        // setErrorMessage(err.response.data.message);
        // setErrorMessageColor(false);
      });
  };

  useEffect(() => {
    setLoadingModule(true);
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/address`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setAddresses(res.data.data);
        setLoadingModule(false);
      });

    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/address/city`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setCities(res.data.data);
      });
  }, []);

  const handleCityChange = (e, { value }) => {
    setCity(value);
    axios
      .get(
        process.env.REACT_APP_CLIENT_API_URL + `/address/district/${value}`,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setDistricts(res.data.data);
      });
  };
  return (
    <div className={styles.myAccountInfo}>
      <LoadingModule
        setLoadingModule={setLoadingModule}
        loadingModule={loadingModule}
      />
      <div className={styles.accountInfosContainer}>
        <p>
          <span>Ad-Soyad:</span>
          {`${userInfo.name} ${userInfo.surname}`}
        </p>
        <p>
          <span>Email:</span>
          {userInfo.email}
        </p>
        <p>
          <span>Telefon Numarası:</span>
          {userInfo.phone_number}
        </p>
        <Modal
          onClose={() => setOpenNew(false)}
          onOpen={() => setOpenNew(true)}
          open={openNew}
          trigger={
            <p
              style={{ marginRight: "0px", width: "97px", textAlign: "center" }}
              className={styles.addAddress}
            >
              <span style={{ marginRight: "0px", textAlign: "center" }}>
                Adres Ekle
              </span>
            </p>
          }
        >
          <Form className={styles.updateAddressModal} onSubmit={addAddress}>
            {errorMessage ? (
              <p
                style={
                  errorMessageColor
                    ? { backgroundColor: "#e0eede" }
                    : { backgroundColor: "#eedede" }
                }
              >
                {errorMessage}
              </p>
            ) : null}
            <Form.Field
              id="form-input-control-first-name"
              control={Input}
              label="Adres Başlığı"
              placeholder="Ev Adresim, Ofis Adresim"
              required
              onChange={(e, { value }) => setAddressTitle(value)}
            />
            <Form.Group widths="equal">
              <Form.Field
                control={Select}
                options={cities}
                label="İl"
                noResultsMessage="İl Bulunamadı"
                required
                placeholder="Seçiniz"
                searchInput={{ id: "form-select-control-gender" }}
                onChange={handleCityChange}
              />
              <Form.Field
                control={Select}
                options={districts}
                label="İlçe"
                required
                noResultsMessage="İlçe Bulunamadı"
                placeholder="Seçiniz"
                searchInput={{ id: "form-select-control-gender" }}
                onChange={(e, { value }) => setDistrict(value)}
              />
            </Form.Group>

            <Form.Field
              id="form-textarea-control-opinion"
              control={TextArea}
              label="Adres Detayı"
              placeholder="Gül sokak, Beyler Sokak, no:60, daire:6..."
              required
              onChange={(e, { value }) => setAddressDetail(value)}
            />
            <Form.Field
              id="form-input-control-error-email"
              control={Input}
              type="text"
              label="Telefon Numarası"
              placeholder="0555-555-5555"
              required
              maxLength="11"
              onChange={(e, { value }) => setPhone(value)}
            />
            <Form.Field
              id="form-button-control-public"
              control={Button}
              content="Ekle"
              type="submit"
            />
          </Form>
        </Modal>
      </div>
      <div className={styles.adddressContainer}>
        {addresses.map((address, index) => (
          <AddressBox
            key={index}
            userInfo={userInfo}
            styles={styles}
            address={address}
            deleteAddress={deleteAddress}
            setLoadingModule={setLoadingModule}
            loadingModule={loadingModule}
          />
        ))}

        {addresses.length > 0 ? null : (
          <h4
            style={{
              width: "100%",
              backgroundColor: "#e7e7e7",
              padding: "30px",
              borderRadius: "5px",
            }}
          >
            Kayıtlı Adres Yok, Lütfen Adres Ekleyin
          </h4>
        )}
      </div>
    </div>
  );
}
