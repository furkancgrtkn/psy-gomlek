import Link from "next/link";
import styles from "../styles/ProductCard.module.css";
export default function ProductCard({ product }) {
  return (
    <div className={styles.productContainer}>
      <Link href={`/gomlekler/urun-detayi/${product.product_id}`}>
        <a>
          <div className={styles.imageWrap}>
            <img
              // src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615419603/product2_nc1ckr.jpg"
              src={product.images.split(",")[0]}
              alt="Gömlek"
              loading="lazy"
            />
          </div>
        </a>
      </Link>
      <div className={styles.productText}>
        <h2>{product.name}</h2>
        <p>
          <span>{product.price} TL </span>
          {product.discounted_price} TL
        </p>
      </div>
      <div className={styles.detailBtn}>
        <Link href={`/gomlekler/urun-detayi/${product.product_id}`}>
          <a>
            <button>ÜRÜN DETAYI</button>
          </a>
        </Link>
      </div>
    </div>
  );
}
