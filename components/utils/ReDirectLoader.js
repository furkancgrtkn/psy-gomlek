import React from "react";
import { useRouter } from "next/router";
import { useEffect } from "react";
import Head from "next/head";

export default function ReDirectLoader() {
  const router = useRouter();
  useEffect(() => {
    router.replace("/giris-yap");
  }, []);
  return (
    <div>
      <Head>
        <title>PSY Gömlek</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <span className="redirectSpan"></span>
    </div>
  );
}
