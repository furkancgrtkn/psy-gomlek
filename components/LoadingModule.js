import { useState } from "react";
import { Modal } from "semantic-ui-react";

export default function LoadingModule({ setLoadingModule, loadingModule }) {
  return (
    <Modal
      onClose={() => setLoadingModule(false)}
      onOpen={() => setLoadingModule(true)}
      open={loadingModule}
      // dimmer=""
      size="small"
      closeOnDimmerClick={false}
    >
      <Modal.Content>
        <div className="loadingModal">
          <div className="lds-default">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
          <p>Lütfen İşlem Devam Ederken Tarayıcıdan Çıkmayınız</p>
        </div>
      </Modal.Content>
    </Modal>
  );
}
