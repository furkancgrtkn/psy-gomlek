import Link from "next/link";
import styles from "../styles/HomeBanner.module.css";

export default function HomeBanner() {
  return (
    <div className={styles.homeBanner}>
      <div className={styles.bannerBox}>
        <Link href="/">
          <a>
            <div className={styles.imageWrap}>
              <img
                src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615418687/6837-erkek-keten-pamuk-ksa-kollu-yazlk-goemlek-o-boyun-boyutu-m-5xl-iyi-kalite-casual-brak-gemi-10-renk-krmz-beyaz-gri-mavi-yesil_hkqnwv.jpg"
                alt="Ayakkabı"
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={styles.bannerBox}>
        <Link href="/">
          <a>
            <div className={styles.imageWrap}>
              <img
                src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615418829/preparing-wedding-groom-buttoning-cufflinks-white-shirt-before-wedding_8353-5804_mfhwbr.jpg"
                alt="Ayakkabı"
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={styles.bannerBox}>
        <Link href="/">
          <a>
            <div className={styles.imageWrap}>
              <img
                src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615418597/41LWoE5DwhL._AC_SY1000__i3sk4g.jpg"
                alt="Ayakkabı"
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={styles.bannerBox}>
        <Link href="/">
          <a>
            <div className={styles.imageWrap}>
              <img
                src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615419603/product2_nc1ckr.jpg"
                alt="Ayakkabı"
              />
            </div>
          </a>
        </Link>
      </div>
      <div className={styles.bannerBox}>
        <Link href="/">
          <a>
            <div className={styles.imageWrap}>
              <img
                src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615418918/10219590713394_che3yq.jpg"
                alt="Ayakkabı"
              />
            </div>
          </a>
        </Link>
      </div>
    </div>
  );
}
