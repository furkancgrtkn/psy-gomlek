import React from "react";
import Navbar from "../Navbar";
import Head from "next/head";
import Footer from "../Footer";

function Layout({ children }) {
  return (
    <div>
      <Head>
        <title>PSY Gömlek</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Navbar />
      <main className="fixedLayout">{children}</main>
      <Footer />
    </div>
  );
}

export default Layout;
