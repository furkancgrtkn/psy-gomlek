import React, { useState } from "react";
import { Modal } from "semantic-ui-react";
import styles from "../styles/Shippings.module.css";
import axios from "axios";
import Cookies from "universal-cookie";
import LoadingModule from "./LoadingModule";

export default function Shippings({ shipping, fetchWaitShips }) {
  const cookies = new Cookies();
  const [loadingModule, setLoadingModule] = useState(false);
  const [shipState, setShipState] = useState(1);
  const [textAreForCargo, setTextAreForCargo] = useState(null);
  const [shipID, setShipID] = useState(null);
  const [textAreForCargo2, setTextAreForCargo2] = useState(null);
  const [shipID2, setShipID2] = useState(null);
  const [notficationShip, setNotficationShip] = useState(null);
  const [notficationShip2, setNotficationShip2] = useState(null);
  const [verfShipping, setVerfShipping] = useState([]);
  const [pastShipping, setPastShipping] = useState([]);
  const [deniedShipping, setDeniedShipping] = useState([]);

  const fetchVerifiedShips = () => {
    setLoadingModule(true);
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/shipping/all_verified`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setVerfShipping(res.data.data);
        setNotficationShip2(null);
        setTimeout(() => {
          setLoadingModule(false);
        }, 1000);
      })
      .catch((err) => console.log(err.response));
  };

  const fetchFinishedShips = () => {
    setLoadingModule(true);

    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/shipping/all_past`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setPastShipping(res.data.data);
        setTimeout(() => {
          setLoadingModule(false);
        }, 1000);
      })
      .catch((err) => console.log(err.response));
  };

  const fetchDeniedShips = () => {
    setLoadingModule(true);

    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/shipping/all_denied`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setDeniedShipping(res.data.data);
        setTimeout(() => {
          setLoadingModule(false);
        }, 1000);
      })
      .catch((err) => console.log(err.response));
  };

  const sendCargoFunc = (e) => {
    e.preventDefault();
    const statData = {
      id: shipID,
      status: 1,
      status_detail: textAreForCargo,
    };
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/shipping/status_code`,
        statData,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setNotficationShip(res.data.message);
        fetchWaitShips();
      })
      .catch((err) => console.log(err.response));
  };

  const sendPastFunc = (e) => {
    e.preventDefault();
    const statData = {
      id: shipID2,
      status: 2,
      status_detail: textAreForCargo2,
    };
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/shipping/status_code`,
        statData,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setNotficationShip2(res.data.message);
        fetchWaitShips();
        fetchVerifiedShips();
      })
      .catch((err) => console.log(err.response));
  };

  const sendDeniedFunc = (e) => {
    const statData = {
      id: e,
      status: 3,
      status_detail:
        "Sipariş iptal edilmiştir detaylı bilgi için iletişime geçebilirsiniz.",
    };
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/shipping/status_code`,
        statData,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        fetchWaitShips();
        fetchVerifiedShips();
      })
      .catch((err) => console.log(err.response));
  };

  return (
    <div className={styles.shipsCont}>
      <LoadingModule
        setLoadingModule={setLoadingModule}
        loadingModule={loadingModule}
      />
      <div className={styles.shipsSections}>
        <ul>
          <li>
            <button
              style={
                shipState === 1
                  ? { color: "white", backgroundColor: "#333333" }
                  : null
              }
              onClick={() => {
                setShipState(1);
                fetchWaitShips();
              }}
            >
              Bekleyen
            </button>
          </li>
          <li>
            <button
              style={
                shipState === 2
                  ? { color: "white", backgroundColor: "#333333" }
                  : null
              }
              onClick={() => {
                setShipState(2);
                fetchVerifiedShips();
              }}
            >
              Kargoya Verilen
            </button>
          </li>
          <li>
            <button
              style={
                shipState === 3
                  ? { color: "white", backgroundColor: "#333333" }
                  : null
              }
              onClick={() => {
                setShipState(3);
                fetchFinishedShips();
              }}
            >
              Tamamlanan
            </button>
          </li>
          <li>
            <button
              style={
                shipState === 4
                  ? { color: "white", backgroundColor: "#333333" }
                  : null
              }
              onClick={() => {
                setShipState(4);
                fetchDeniedShips();
              }}
            >
              İptal Edilen
            </button>
          </li>
        </ul>
      </div>
      {shipState === 1
        ? shipping
          ? shipping.map((shipwait, index) => (
              <div key={index} className={styles.shipsCard}>
                <div className={styles.shipsCardCustomer}>
                  <ul>
                    <li>
                      <b>Müşteri:</b> {shipwait.name} {shipwait.surname}
                    </li>
                    <li>
                      <b>ADRES:</b> {shipwait.address_info.city_name}/
                      {shipwait.address_info.district_name}{" "}
                      {shipwait.address_info.detail}
                    </li>
                    <li>
                      <b>TELEFON NUMARASI:</b>
                      {shipwait.phone_number}
                    </li>
                    <li>
                      <b>MAİL ADRESİ:</b> {shipwait.email}
                    </li>
                    <li>
                      <b>ÖDENECEK FİYAT:</b> {shipwait.total_price}TL
                    </li>
                  </ul>
                </div>

                <div className={styles.allCardProducts}>
                  {shipwait.basket.map((basketItem, index) => (
                    <div key={index} className={styles.shipsCardProduct}>
                      <div className={styles.shipsCardProductImage}>
                        <img src={basketItem.images.split(",")[0]} />
                      </div>
                      <ul>
                        <li>
                          <b>ÜRÜN:</b> {basketItem.name}
                        </li>
                        <li>
                          <b>ÜRÜN KODU:</b> {basketItem.code}
                        </li>
                        <li>
                          <b>ÜRÜN RENGİ:</b> {basketItem.color_name}
                        </li>
                        <li>
                          <b>ÜRÜN BEDENİ:</b> {basketItem.size_product} Beden
                        </li>
                        <li>
                          <b>ÜRÜN ADEDİ:</b> {basketItem.count} ADET
                        </li>
                        <li>
                          <b>ÜRÜNÜN FİYATI:</b> {basketItem.discounted_price}TL
                        </li>
                      </ul>
                    </div>
                  ))}
                </div>
                <div className={styles.cardProductsBtns}>
                  <Modal
                    closeIcon
                    onClose={() => setNotficationShip(null)}
                    className="cardProdModal"
                    trigger={<button>KARGOYA VERİLDİ</button>}
                  >
                    <form
                      onSubmit={(e) => sendCargoFunc(e)}
                      className={styles.cardProductsForm}
                    >
                      <label>Kargo Takip Numarası</label>
                      {notficationShip ? (
                        <label>{notficationShip}</label>
                      ) : null}
                      <textarea
                        required
                        onChange={(e) => {
                          setTextAreForCargo(e.target.value);
                          setShipID(shipwait.ship_id);
                        }}
                      ></textarea>
                      <button type="submit">ONAYLA</button>
                    </form>
                  </Modal>
                  <button
                    onClick={() => {
                      sendDeniedFunc(shipwait.ship_id);
                    }}
                  >
                    İPTAL ET
                  </button>
                </div>
              </div>
            ))
          : null
        : null}
      {shipState === 2
        ? verfShipping.map((shipwait, index) => (
            <div key={index} className={styles.shipsCard}>
              <div className={styles.shipsCardCustomer}>
                <ul>
                  <li>
                    <b>Müşteri:</b> {shipwait.name} {shipwait.surname}
                  </li>
                  <li>
                    <b>ADRES:</b> {shipwait.address_info.city_name}/
                    {shipwait.address_info.district_name}{" "}
                    {shipwait.address_info.detail}
                  </li>
                  <li>
                    <b>TELEFON NUMARASI:</b>
                    {shipwait.phone_number}
                  </li>
                  <li>
                    <b>MAİL ADRESİ:</b> {shipwait.email}
                  </li>
                  <li>
                    <b>ÖDENECEK FİYAT:</b> {shipwait.total_price}TL
                  </li>
                </ul>
              </div>

              <div className={styles.allCardProducts}>
                {shipwait.basket.map((basketItem, index) => (
                  <div key={index} className={styles.shipsCardProduct}>
                    <div className={styles.shipsCardProductImage}>
                      <img src={basketItem.images.split(",")[0]} />
                    </div>
                    <ul>
                      <li>
                        <b>ÜRÜN:</b> {basketItem.name}
                      </li>
                      <li>
                        <b>ÜRÜN KODU:</b> {basketItem.code}
                      </li>
                      <li>
                        <b>ÜRÜN RENGİ:</b> {basketItem.color_name}
                      </li>
                      <li>
                        <b>ÜRÜN BEDENİ:</b> {basketItem.size_product} Beden
                      </li>
                      <li>
                        <b>ÜRÜN ADEDİ:</b> {basketItem.count} ADET
                      </li>
                      <li>
                        <b>ÜRÜNÜN FİYATI:</b> {basketItem.discounted_price}TL
                      </li>
                    </ul>
                  </div>
                ))}
              </div>
              <div className={styles.cardProductsBtns}>
                <Modal
                  closeIcon
                  onClose={() => setNotficationShip2(null)}
                  className="cardProdModal"
                  trigger={<button>TESLİM EDİLDİ</button>}
                >
                  <form
                    onSubmit={(e) => sendPastFunc(e)}
                    className={styles.cardProductsForm}
                  >
                    <label>Açıklama</label>
                    {notficationShip2 ? (
                      <label>{notficationShip2}</label>
                    ) : null}
                    <textarea
                      required
                      onChange={(e) => {
                        setTextAreForCargo2(e.target.value);
                        setShipID2(shipwait.ship_id);
                      }}
                    ></textarea>
                    <button type="submit">ONAYLA</button>
                  </form>
                </Modal>
                <button
                  onClick={() => {
                    sendDeniedFunc(shipwait.ship_id);
                  }}
                >
                  İPTAL ET
                </button>
              </div>
            </div>
          ))
        : null}
      {shipState === 3
        ? pastShipping.map((shipwait, index) => (
            <div key={index} className={styles.shipsCard}>
              <div className={styles.shipsCardCustomer}>
                <ul>
                  <li>
                    <b>Müşteri:</b> {shipwait.name} {shipwait.surname}
                  </li>
                  <li>
                    <b>ADRES:</b> {shipwait.address_info.city_name}/
                    {shipwait.address_info.district_name}{" "}
                    {shipwait.address_info.detail}
                  </li>
                  <li>
                    <b>TELEFON NUMARASI:</b>
                    {shipwait.phone_number}
                  </li>
                  <li>
                    <b>MAİL ADRESİ:</b> {shipwait.email}
                  </li>
                  <li>
                    <b>ÖDENECEK FİYAT:</b> {shipwait.total_price}TL
                  </li>
                </ul>
              </div>

              <div className={styles.allCardProducts}>
                {shipwait.basket.map((basketItem, index) => (
                  <div key={index} className={styles.shipsCardProduct}>
                    <div className={styles.shipsCardProductImage}>
                      <img src={basketItem.images.split(",")[0]} />
                    </div>
                    <ul>
                      <li>
                        <b>ÜRÜN:</b> {basketItem.name}
                      </li>
                      <li>
                        <b>ÜRÜN KODU:</b> {basketItem.code}
                      </li>
                      <li>
                        <b>ÜRÜN RENGİ:</b> {basketItem.color_name}
                      </li>
                      <li>
                        <b>ÜRÜN BEDENİ:</b> {basketItem.size_product} Beden
                      </li>
                      <li>
                        <b>ÜRÜN ADEDİ:</b> {basketItem.count} ADET
                      </li>
                      <li>
                        <b>ÜRÜNÜN FİYATI:</b> {basketItem.discounted_price}TL
                      </li>
                    </ul>
                  </div>
                ))}
              </div>
            </div>
          ))
        : null}
      {shipState === 4
        ? deniedShipping.map((shipwait, index) => (
            <div key={index} className={styles.shipsCard}>
              <div className={styles.shipsCardCustomer}>
                <ul>
                  <li>
                    <b>Müşteri:</b> {shipwait.name} {shipwait.surname}
                  </li>
                  <li>
                    <b>ADRES:</b> {shipwait.address_info.city_name}/
                    {shipwait.address_info.district_name}{" "}
                    {shipwait.address_info.detail}
                  </li>
                  <li>
                    <b>TELEFON NUMARASI:</b>
                    {shipwait.phone_number}
                  </li>
                  <li>
                    <b>MAİL ADRESİ:</b> {shipwait.email}
                  </li>
                  <li>
                    <b>ÖDENECEK FİYAT:</b> {shipwait.total_price}TL
                  </li>
                </ul>
              </div>

              <div className={styles.allCardProducts}>
                {shipwait.basket.map((basketItem, index) => (
                  <div key={index} className={styles.shipsCardProduct}>
                    <div className={styles.shipsCardProductImage}>
                      <img src={basketItem.images.split(",")[0]} />
                    </div>
                    <ul>
                      <li>
                        <b>ÜRÜN:</b> {basketItem.name}
                      </li>
                      <li>
                        <b>ÜRÜN KODU:</b> {basketItem.code}
                      </li>
                      <li>
                        <b>ÜRÜN RENGİ:</b> {basketItem.color_name}
                      </li>
                      <li>
                        <b>ÜRÜN BEDENİ:</b> {basketItem.size_product} Beden
                      </li>
                      <li>
                        <b>ÜRÜN ADEDİ:</b> {basketItem.count} ADET
                      </li>
                      <li>
                        <b>ÜRÜNÜN FİYATI:</b> {basketItem.discounted_price}TL
                      </li>
                    </ul>
                  </div>
                ))}
              </div>
            </div>
          ))
        : null}
    </div>
  );
}
