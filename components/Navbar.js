import Link from "next/link";
import React, { useEffect, useState } from "react";
import useScrollPosition from "../hooks/useScrollPosition";
import styles from "../styles/Navbar.module.css";
import { FiShoppingCart } from "react-icons/fi";
import { VscAccount } from "react-icons/vsc";
import Cookies from "universal-cookie";

export default function Navbar() {
  const cookies = new Cookies();
  const scrollPos = useScrollPosition();
  const [pos, setPos] = useState(0);
  useEffect(() => {
    setPos(scrollPos);
  }, [scrollPos]);
  return (
    <nav
      className={
        pos > 1 ? `${styles.navbarScroll} ${styles.navbar}` : styles.navbar
      }
    >
      <div className={styles.navbarInner}>
        <div className={styles.navbarInnerLeft}>
          <Link href="/">
            <a>
              <div className={styles.navbarLogo}>
                <img
                  style={{ width: "41px", height: "32.5px" }}
                  src="/images/logo.png"
                  alt="PSY Gömlek"
                />
              </div>
              <h1>PSY GÖMLEK</h1>
            </a>
          </Link>
        </div>
        <div className={styles.navbarInnerRight}>
          <ul>
            <Link href="/gomlekler/1">
              <a>
                <li>
                  <span>GÖMLEKLER</span>
                </li>
              </a>
            </Link>

            <>
              <Link href="/sepetim">
                <a>
                  <li>
                    <FiShoppingCart />{" "}
                  </li>
                </a>
              </Link>
              <Link href="/hesabim">
                <a>
                  <li>
                    <VscAccount />{" "}
                  </li>
                </a>
              </Link>
            </>
          </ul>
        </div>
      </div>
    </nav>
  );
}
