import styles from "../../styles/OrderCompletedPage.module.css";
import { FaCheck } from "react-icons/fa";
import Link from "next/link";

export default function OrderCompletedPage() {
  return (
    <div className={styles.orderCompletedPage}>
      <div className={styles.sellerCompletedSectionWrapper}>
        <div className={styles.sellerCompletedSectionImgContainer}>
          <div className={styles.sellerCompletedSectionImgInner}>
            <FaCheck />
          </div>
        </div>
        <h1>SİPARİŞ BAŞARIYLA VERİLDİ</h1>
        <Link className={styles.sellerCompletedSectionLink} href="/">
          <a>ANASAYFA</a>
        </Link>
        <p>
          Açıklama; Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
          diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
          erat, sed diam voluptua. At vero eos
        </p>
      </div>
    </div>
  );
}
