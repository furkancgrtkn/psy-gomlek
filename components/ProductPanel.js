import axios from "axios";
import React, { useEffect, useState } from "react";
import { Dropdown, Modal } from "semantic-ui-react";
import Cookies from "universal-cookie";
import styles from "../styles/ProductPanel.module.css";

export default function ProductPanel({ products, fetchProducts }) {
  const sleeves = [
    { key: 1, text: "Kısa Kollu", value: 0 },
    { key: 2, text: "Uzun Kollu", value: 1 },
  ];
  const cookies = new Cookies();
  const fits = [
    { key: 1, text: "Slim Fit", value: 1 },
    { key: 2, text: "Klasik Fit", value: 0 },
  ];

  const [allColors, setAllColors] = useState([]);
  const [name, setName] = useState(null);
  const [size, setSize] = useState([]);
  const [fit, setFit] = useState(null);
  const [detail, setDetail] = useState(null);
  const [images, setImages] = useState(null);
  const [sleeve, setSleeve] = useState(null);
  const [price, setPrice] = useState(null);
  const [discounted_price, setDiscPrice] = useState(null);
  const [color, setColor] = useState(null);
  const [code, setCode] = useState(null);
  const [notification, setNotfication] = useState(null);

  const [name2, setName2] = useState(null);
  const [size2, setSize2] = useState([]);
  const [fit2, setFit2] = useState(null);
  const [detail2, setDetail2] = useState(null);
  const [images2, setImages2] = useState(null);
  const [sleeve2, setSleeve2] = useState(null);
  const [price2, setPrice2] = useState(null);
  const [discounted_price2, setDiscPrice2] = useState(null);
  const [color2, setColor2] = useState(null);
  const [code2, setCode2] = useState(null);
  const [updatedProdId, setUpdatedProdId] = useState(null);
  useEffect(() => {
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + "/products/get_colors")
      .then((resp) => {
        setAllColors(resp.data.data);
      });
  }, []);

  const createProductFunc = (e) => {
    e.preventDefault();
    const newProduct = {
      name: name,
      size: size.join(","),
      fit: fit,
      detail: detail,
      images: images,
      sleeve: sleeve,
      price: parseInt(price),
      discounted_price: parseInt(discounted_price),
      color: color,
      code: code,
    };
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/products/create`,
        newProduct,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setNotfication(res.data.message);
        fetchProducts();
      })
      .catch((err) => setNotfication(err.response.data.error));
  };

  const deleteProduct = (id) => {
    const idData = {
      id: id,
    };
    axios
      .post(process.env.REACT_APP_CLIENT_API_URL + `/products/delete`, idData, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        fetchProducts();
      });
  };

  const updateProductd = (id, e) => {
    e.preventDefault();
    const newProductUpdate = {
      id: id,
      name: name2,
      size: size2.join(","),
      fit: fit2,
      detail: detail2,
      images: images2,
      sleeve: sleeve2,
      price: parseInt(price2),
      discounted_price: parseInt(discounted_price2),
      color: color2,
      code: code2,
    };
    axios
      .put(
        process.env.REACT_APP_CLIENT_API_URL + `/products/update`,
        newProductUpdate,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        fetchProducts();
      });
  };

  return (
    <div className={styles.productPanel}>
      <div className={styles.productPanelFormContainer}>
        <h5>ÜRÜN EKLE</h5>
        <form
          onSubmit={(e) => createProductFunc(e)}
          className={styles.productPanelForm}
        >
          <div className={styles.productPanelFormDiv}>
            <label>ÜRÜN ADI</label>
            <input required onChange={(e) => setName(e.target.value)}></input>
          </div>
          <div className={styles.productPanelFormDiv}>
            <label>ÜRÜN DETAYI</label>
            <textarea
              required
              onChange={(e) => setDetail(e.target.value)}
            ></textarea>
          </div>
          <div className={styles.productPanelFormDivCheck}>
            <label>BEDEN SEÇENEKLERİ</label>
            <div className={styles.productPanelFormDivChecks}>
              <div>
                <input
                  value="XS"
                  onChange={(e) => {
                    if (!size.includes(e.target.value)) {
                      setSize([...size, e.target.value]);
                    } else {
                      setSize(size.filter((item) => item !== e.target.value));
                    }
                  }}
                  type="checkbox"
                ></input>
                <span>XS</span>
              </div>
              <div>
                <input
                  value="S"
                  onChange={(e) => {
                    if (!size.includes(e.target.value)) {
                      setSize([...size, e.target.value]);
                    } else {
                      setSize(size.filter((item) => item !== e.target.value));
                    }
                  }}
                  type="checkbox"
                ></input>
                <span>S</span>
              </div>
              <div>
                <input
                  value="M"
                  onChange={(e) => {
                    if (!size.includes(e.target.value)) {
                      setSize([...size, e.target.value]);
                    } else {
                      setSize(size.filter((item) => item !== e.target.value));
                    }
                  }}
                  type="checkbox"
                ></input>
                <span>M</span>
              </div>
              <div>
                <input
                  value="L"
                  onChange={(e) => {
                    if (!size.includes(e.target.value)) {
                      setSize([...size, e.target.value]);
                    } else {
                      setSize(size.filter((item) => item !== e.target.value));
                    }
                  }}
                  type="checkbox"
                ></input>
                <span>L</span>
              </div>
              <div>
                <input
                  value="XL"
                  onChange={(e) => {
                    if (!size.includes(e.target.value)) {
                      setSize([...size, e.target.value]);
                    } else {
                      setSize(size.filter((item) => item !== e.target.value));
                    }
                  }}
                  type="checkbox"
                ></input>
                <span>XL</span>
              </div>
              <div>
                <input
                  value="XXL"
                  onChange={(e) => {
                    if (!size.includes(e.target.value)) {
                      setSize([...size, e.target.value]);
                    } else {
                      setSize(size.filter((item) => item !== e.target.value));
                    }
                  }}
                  type="checkbox"
                ></input>
                <span>XXL</span>
              </div>
            </div>
          </div>
          <div className={styles.productPanelFormDivDrop}>
            <label>KESİM</label>
            <Dropdown
              className="fitDropdown2"
              options={fits}
              onChange={(e, { value }) => setFit(value)}
              placeholder="Kesim Seçin"
              selection
            />
          </div>
          <div className={styles.productPanelFormDiv}>
            <label>GÖRSELLER</label>
            <textarea
              required
              onChange={(e) => setImages(e.target.value)}
            ></textarea>
          </div>
          <div className={styles.productPanelFormDivDrop}>
            <label>KOL TİPİ</label>
            <Dropdown
              className="fitDropdown2"
              options={sleeves}
              onChange={(e, { value }) => setSleeve(value)}
              placeholder="Kalıp Seçin"
              selection
            />
          </div>
          <div className={styles.productPanelFormDiv}>
            <label>FİYAT</label>
            <input
              required
              onChange={(e) => setPrice(e.target.value)}
              type="number"
            ></input>
          </div>

          <div className={styles.productPanelFormDiv}>
            <label>İNDİRİMLİ FİYAT</label>
            <input
              required
              onChange={(e) => setDiscPrice(e.target.value)}
              type="number"
            ></input>
          </div>

          <div className={styles.productPanelFormDiv}>
            <label>ÜRÜN KODU</label>
            <input required onChange={(e) => setCode(e.target.value)}></input>
          </div>

          <div className={styles.productPanelFormDivDrop}>
            <label>RENK</label>
            <Dropdown
              className="fitDropdown2"
              options={allColors}
              onChange={(e, { value }) => setColor(value)}
              placeholder="Kalıp Seçin"
              selection
            />
          </div>
          <button type="submit">ONAYLA</button>
          {notification ? (
            <p
              style={{
                marginBottom: "20px",
                padding: "0px 10px",
                textAlign: "center",
              }}
            >
              {notification}
            </p>
          ) : null}
        </form>
      </div>
      <div className={styles.allProds}>
        <h5>TÜM ÜRÜNLER</h5>
        <div className={styles.allProdsCards}>
          {products
            ? products.map((product, index) => (
                <div key={index} className={styles.allProdsCard}>
                  <div className={styles.allProdsCardImage}>
                    <img src={product.images.split(",")[0]} />
                  </div>
                  <ul>
                    <li>
                      <b>ÜRÜN:</b> {product.name}
                    </li>
                    <li>
                      <b>ÜRÜN DETAYI:</b> {product.detail}
                    </li>
                    <li>
                      <b>BEDENLER:</b> {product.size}
                    </li>
                    <li>
                      <b>KESİM:</b> {product.fit === 1 ? "Slim Fit" : "Klasik"}
                    </li>
                    <li>
                      <b>KOL TİPİ:</b>{" "}
                      {product.sleeve === 1 ? "Uzun Kollu" : "Kısa Kollu"}
                    </li>
                    <li>
                      <b>RENK:</b> {product.color_name}
                    </li>
                    <li>
                      <b>FİYAT:</b> {product.price}TL
                    </li>
                    <li>
                      <b>İNDİRİMLİ FİYAT:</b> {product.discounted_price}TL
                    </li>
                    <li>
                      <b>ÜRÜN KODU:</b> {product.code}
                    </li>
                    <li>
                      <Modal
                        className="editproductmodal"
                        trigger={
                          <button
                            onClick={() => {
                              setName2(product.name);
                              setDetail2(product.detail);
                              setCode2(product.code);
                              setColor2(product.color);
                              setDiscPrice2(product.discounted_price);
                              setPrice2(product.price);
                              setFit2(product.fit);
                              setSleeve2(product.sleeve);
                              setImages2(product.images);
                              setSize2(product.size.split(","));
                              setUpdatedProdId(product.product_id);
                            }}
                          >
                            DÜZENLE
                          </button>
                        }
                      >
                        <form
                          style={{ marginBottom: "0px" }}
                          onSubmit={(e) =>
                            updateProductd(product.product_id, e)
                          }
                          className={styles.productPanelForm}
                        >
                          <div className={styles.productPanelFormDiv}>
                            <label>ÜRÜN ADI</label>
                            <input
                              required
                              defaultValue={product.name}
                              onChange={(e) => setName2(e.target.value)}
                            ></input>
                          </div>
                          <div className={styles.productPanelFormDiv}>
                            <label>ÜRÜN DETAYI</label>
                            <textarea
                              required
                              defaultValue={product.detail}
                              onChange={(e) => setDetail2(e.target.value)}
                            ></textarea>
                          </div>
                          <div className={styles.productPanelFormDivCheck}>
                            <label>BEDEN SEÇENEKLERİ</label>
                            <div className={styles.productPanelFormDivChecks}>
                              <div>
                                <input
                                  value="XS"
                                  defaultChecked={size2.includes("XS")}
                                  onChange={(e) => {
                                    if (!size2.includes(e.target.value)) {
                                      setSize2([...size2, e.target.value]);
                                    } else {
                                      setSize2(
                                        size2.filter(
                                          (item) => item !== e.target.value
                                        )
                                      );
                                    }
                                  }}
                                  type="checkbox"
                                ></input>
                                <span>XS</span>
                              </div>
                              <div>
                                <input
                                  value="S"
                                  defaultChecked={size2.includes("S")}
                                  onChange={(e) => {
                                    if (!size2.includes(e.target.value)) {
                                      setSize2([...size2, e.target.value]);
                                    } else {
                                      setSize2(
                                        size2.filter(
                                          (item) => item !== e.target.value
                                        )
                                      );
                                    }
                                  }}
                                  type="checkbox"
                                ></input>
                                <span>S</span>
                              </div>
                              <div>
                                <input
                                  value="M"
                                  defaultChecked={size2.includes("M")}
                                  onChange={(e) => {
                                    if (!size2.includes(e.target.value)) {
                                      setSize2([...size2, e.target.value]);
                                    } else {
                                      setSize2(
                                        size2.filter(
                                          (item) => item !== e.target.value
                                        )
                                      );
                                    }
                                  }}
                                  type="checkbox"
                                ></input>
                                <span>M</span>
                              </div>
                              <div>
                                <input
                                  value="L"
                                  defaultChecked={size2.includes("L")}
                                  onChange={(e) => {
                                    if (!size2.includes(e.target.value)) {
                                      setSize2([...size2, e.target.value]);
                                    } else {
                                      setSize2(
                                        size2.filter(
                                          (item) => item !== e.target.value
                                        )
                                      );
                                    }
                                  }}
                                  type="checkbox"
                                ></input>
                                <span>L</span>
                              </div>
                              <div>
                                <input
                                  value="XL"
                                  defaultChecked={size2.includes("XL")}
                                  onChange={(e) => {
                                    if (!size2.includes(e.target.value)) {
                                      setSize2([...size2, e.target.value]);
                                    } else {
                                      setSize2(
                                        size2.filter(
                                          (item) => item !== e.target.value
                                        )
                                      );
                                    }
                                  }}
                                  type="checkbox"
                                ></input>
                                <span>XL</span>
                              </div>
                              <div>
                                <input
                                  value="XXL"
                                  defaultChecked={size2.includes("XXL")}
                                  onChange={(e) => {
                                    if (!size2.includes(e.target.value)) {
                                      setSize2([...size2, e.target.value]);
                                    } else {
                                      setSize2(
                                        size2.filter(
                                          (item) => item !== e.target.value
                                        )
                                      );
                                    }
                                  }}
                                  type="checkbox"
                                ></input>
                                <span>XXL</span>
                              </div>
                            </div>
                          </div>
                          <div className={styles.productPanelFormDivDrop}>
                            <label>KESİM</label>
                            <Dropdown
                              defaultValue={product.fit}
                              className="fitDropdown2"
                              options={fits}
                              onChange={(e, { value }) => setFit2(value)}
                              placeholder="Kesim Seçin"
                              selection
                            />
                          </div>
                          <div className={styles.productPanelFormDiv}>
                            <label>GÖRSELLER</label>
                            <textarea
                              defaultValue={product.images}
                              required
                              onChange={(e) => setImages2(e.target.value)}
                            ></textarea>
                          </div>
                          <div className={styles.productPanelFormDivDrop}>
                            <label>KOL TİPİ</label>
                            <Dropdown
                              defaultValue={product.sleeve}
                              className="fitDropdown2"
                              options={sleeves}
                              onChange={(e, { value }) => setSleeve2(value)}
                              placeholder="Kalıp Seçin"
                              selection
                            />
                          </div>
                          <div className={styles.productPanelFormDiv}>
                            <label>FİYAT</label>
                            <input
                              defaultValue={product.price}
                              required
                              onChange={(e) => setPrice2(e.target.value)}
                              type="number"
                            ></input>
                          </div>

                          <div className={styles.productPanelFormDiv}>
                            <label>İNDİRİMLİ FİYAT</label>
                            <input
                              defaultValue={product.discounted_price}
                              required
                              onChange={(e) => setDiscPrice2(e.target.value)}
                              type="number"
                            ></input>
                          </div>

                          {/* <div className={styles.productPanelFormDiv}>
                            <label>ÜRÜN KODU</label>
                            <input
                              defaultValue={product.code}
                              required
                              onChange={(e) => setCode2(e.target.value)}
                            ></input>
                          </div> */}

                          <div className={styles.productPanelFormDivDrop}>
                            <label>RENK</label>
                            <Dropdown
                              defaultValue={product.color}
                              className="fitDropdown2"
                              options={allColors}
                              onChange={(e, { value }) => setColor2(value)}
                              placeholder="Kalıp Seçin"
                              selection
                            />
                          </div>
                          <button type="submit">ONAYLA</button>
                          {notification ? (
                            <p
                              style={{
                                marginBottom: "20px",
                                padding: "0px 10px",
                                textAlign: "center",
                              }}
                            >
                              {notification}
                            </p>
                          ) : null}
                        </form>
                      </Modal>
                    </li>
                    {/* <li>
                      <button onClick={() => deleteProduct(product.product_id)}>
                        SİL
                      </button>
                    </li> */}
                  </ul>
                </div>
              ))
            : null}
        </div>
      </div>
    </div>
  );
}
