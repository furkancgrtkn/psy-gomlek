import styles from "../styles/MyOrders.module.css";
import { BsTrash } from "react-icons/bs";
import { useState, useEffect } from "react";
import { Form, Select } from "semantic-ui-react";
import axios from "axios";
import Cookies from "universal-cookie";
import LoadingModule from "./LoadingModule";

export default function MyOrders() {
  const cookies = new Cookies();

  const sortOptions = [
    { text: "Bekleyen Sipariş", value: "waiting" },
    { text: "Onaylanan Sipariş", value: "verified" },
    { text: "Geçmiş Sipariş", value: "past" },
    { text: "İptal Sipariş", value: "denied" },
  ];

  const months = [
    "Ocak",
    "Şubat",
    "Mart",
    "Nisan",
    "Mayıs",
    "Haziran",
    "Temmuz",
    "Ağustos",
    "Eylül",
    "Ekim",
    "Kasım",
    "Aralık",
  ];

  const [loadingModule, setLoadingModule] = useState(false);

  const [userInfo, setUserInfo] = useState({});
  const [orders, setOrders] = useState([]);
  const onChangeSort = (e, { value }) => {
    setLoadingModule(true);
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/shipping/${value}`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setOrders(res.data.data);
        setLoadingModule(false);
      });
  };

  useEffect(() => {
    setUserInfo(cookies.get("psyJWTUser"));
    onChangeSort(1, { value: "waiting" });
  }, []);

  const formatDate = (shipDate) => {
    let date = new Date(shipDate);

    return `${date.getDate()} ${months[date.getMonth()]}`;
  };

  return (
    <div className={styles.myOrders}>
      <LoadingModule
        setLoadingModule={setLoadingModule}
        loadingModule={loadingModule}
      />
      <div className={styles.pageTitle}>
        <h1>Siparişlerim</h1>
        <Form.Field
          className={styles.sortMyOrderDropdown}
          control={Select}
          options={sortOptions}
          required
          onChange={onChangeSort}
          placeholder="Sıralama"
          defaultValue="waiting"
        />
      </div>
      {orders.map((order, index) => (
        <div key={index} className={styles.orderWrapper}>
          <div className={styles.orderTitles}>
            <h3>
              <span>Sipariş Tarihi</span> <br />
              {formatDate(order.created_at)}
            </h3>
            <h3>
              <span>Sipariş Özeti</span> <br />
              {order.basket.length} ürün
            </h3>
            <h3>
              <span>Sipariş Durumu</span> <br />
              {order.status_detail}
            </h3>
            <h3>
              <span>Alıcı</span> <br />
              {`${userInfo.name} ${userInfo.surname}`}
            </h3>
            <h3>
              <span>Tutar</span> <br />
              {order.total_price} TL
            </h3>
          </div>
          <div className={styles.orderInfoContainer}>
            {order.basket.map((product, index) => (
              <div key={index} className={styles.shipsCardProduct}>
                <div className={styles.shipsCardProductImage}>
                  <img src={product.images.split(",")[0]} />
                </div>
                <ul>
                  <li>
                    <b>ÜRÜN:</b> {product.name}
                  </li>
                  <li>
                    <b>ÜRÜN KODU:</b> {product.code}
                  </li>
                  <li>
                    <b>ÜRÜN RENGİ:</b> {product.color_name}
                  </li>
                  <li>
                    <b>ÜRÜN BEDENİ:</b> {product.size_product} Beden
                  </li>
                  <li>
                    <b>ÜRÜN ADEDİ:</b> {product.count} ADET
                  </li>
                  <li>
                    <b>ÜRÜNÜN FİYATI:</b> {product.discounted_price} TL
                  </li>
                </ul>
              </div>
            ))}
          </div>
        </div>
      ))}

      {orders.length > 0 ? null : (
        <h4
          style={{
            width: "100%",
            backgroundColor: "#e7e7e7",
            padding: "30px",
            borderRadius: "5px",
          }}
        >
          Sipariş Bulunamadı
        </h4>
      )}
    </div>
  );
}
