import Link from "next/link";
import styles from "../styles/HomeTopSwiper.module.css";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination, Autoplay, A11y } from "swiper";
import "swiper/swiper-bundle.css";
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);
export default function HomeTopSwiper() {
  return (
    <div className={styles.homeTopSwiper}>
      <Swiper
        slidesPerView={1}
        className="homeSwiper"
        loop={true}
        autoplay={{ delay: 2500 }}
        navigation
      >
        <SwiperSlide>
          <Link href="/">
            <a>
              <div className={styles.imageWrap}>
                <img
                  src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615412012/855fc69e-b765-4bcf-8a13-49022f047648_ntfkhs.jpg"
                  alt="Kampanya"
                />
              </div>
            </a>
          </Link>
        </SwiperSlide>
        <SwiperSlide>
          <Link href="/">
            <a>
              <div className={styles.imageWrap}>
                <img
                  src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615412012/855fc69e-b765-4bcf-8a13-49022f047648_ntfkhs.jpg"
                  alt="Kampanya"
                />
              </div>
            </a>
          </Link>
        </SwiperSlide>
      </Swiper>
    </div>
  );
}
