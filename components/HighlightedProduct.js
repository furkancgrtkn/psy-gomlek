import styles from "../styles/HighlightedProduct.module.css";
import ProductCard from "./ProductCard";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
import SwiperCore, { Navigation, Pagination, Autoplay, A11y } from "swiper";
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);

export default function HighlightedProduct({ perView, title, products }) {
  return (
    <div className={styles.highlightedProduct}>
      <div className={styles.title}>
        <h1>{title}</h1>
      </div>
      <div className={styles.highlightedSwiper}>
        <Swiper
          className="homeProductSwiper"
          slidesPerView={perView}
          loop={true}
          autoplay={{ delay: 2500 }}
          navigation
          spaceBetween={40}
        >
          {products
            ? products.map((product, index) => (
                <SwiperSlide key={index}>
                  <ProductCard product={product} />
                </SwiperSlide>
              ))
            : null}
        </Swiper>
      </div>
    </div>
  );
}
