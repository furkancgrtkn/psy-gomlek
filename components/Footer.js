import React from "react";
import styles from "../styles/Footer.module.css";
import Link from "next/link";

import {
  FaInstagram,
  FaFacebookSquare,
  FaTwitter,
  FaLinkedin,
  FaWhatsapp,
  FaGoogle,
} from "react-icons/fa";

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.footerInner}>
        <div className={styles.footerInnerLeft}>
          <Link href="/">
            <a>
              <div className={styles.footerInnerLeftLogo}>
                <div className={styles.footerLogo}>
                  <img
                    style={{ width: "41px", height: "32.5px" }}
                    src="/images/logo.png"
                    alt="PSY Gömlek"
                  />
                </div>
                <h1>PSY GÖMLEK</h1>
              </div>
            </a>
          </Link>
          <div className={styles.footerInnerLeftIcons}>
            <h4>Mağaza Adresi</h4>
            <p>
              Tahtakale, Faruk Nafız Çamlıbel Sk. No: 2/12, 34320
              Avcılar/İstanbul
            </p>
          </div>
          <div className={styles.footerInnerLeftIcons}>
            <h4>Sosyal Medya Hesaplarımız</h4>
            <ul>
              <Link href="/">
                <a>
                  <li>
                    <FaInstagram />
                  </li>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <li>
                    <FaFacebookSquare />
                  </li>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <li>
                    <FaTwitter />
                  </li>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <li>
                    <FaLinkedin />
                  </li>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <li>
                    <FaWhatsapp />
                  </li>
                </a>
              </Link>
              <Link href="/">
                <a>
                  <li>
                    <FaGoogle />
                  </li>
                </a>
              </Link>
            </ul>
          </div>
        </div>

        <div className={styles.footerInnerRight}>
          <div className={styles.footerInnerRightTop}>
            <div className={styles.footerInnerRightMap}>
              <h4>Site Haritası</h4>
              <ul>
                <Link href="/">
                  <a>
                    <li>Ana Sayfa</li>
                  </a>
                </Link>

                <Link href="/">
                  <a>
                    <li>Gömlekler</li>
                  </a>
                </Link>

                <Link href="/">
                  <a>
                    <li>İletişim</li>
                  </a>
                </Link>

                <Link href="/">
                  <a>
                    <li>Hesabım</li>
                  </a>
                </Link>

                <Link href="/">
                  <a>
                    <li>Sepetim</li>
                  </a>
                </Link>
              </ul>
            </div>
            <div className={styles.footerInnerRightMap}>
              <h4>Kurumsal</h4>
              <ul>
                <li>Mesafeli Satış Sözleşmesi</li>

                <li>Ön Bilgilendirme Formu</li>

                <li>Üyelik Sözleşmesi</li>

                <li>Kullanım Koşulları</li>

                <li>Gizlilik Politikası</li>
              </ul>
            </div>
          </div>
          <div className={styles.footerInnerRightBottom}>
            <h6>Copyright &copy; PSY Gömlek 2021. Tüm hakları saklıdır.</h6>
          </div>
        </div>
      </div>
    </footer>
  );
}
