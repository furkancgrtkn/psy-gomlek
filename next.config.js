module.exports = {
  images: {
    domains: ["res.cloudinary.com"],
    deviceSizes: [640, 750, 828, 1080, 1200],
  },
  async redirects() {
    return [
      {
        source: "/gomlekler",
        destination: "/gomlekler/1",
        permanent: true,
      },
    ];
  },
  env: {
    REACT_APP_CLIENT_API_URL:
      "https://psy-backend-ii4oi.ondigitalocean.app/api",
    NEXT_APP_API_URL: "https://psy-backend-ii4oi.ondigitalocean.app/api",
  },
  // env: {
  //   REACT_APP_CLIENT_API_URL: "http://localhost:5000/api",
  //   NEXT_APP_API_URL: "http://localhost:5000/api",
  // },
};

// env: {
//   REACT_APP_CLIENT_API_URL:
//     "https://psy-backend-ii4oi.ondigitalocean.app/api",
//   NEXT_APP_API_URL: "https://psy-backend-ii4oi.ondigitalocean.app/api",
// },

// env: {
//   REACT_APP_CLIENT_API_URL: "http://localhost:5000/api",
//   NEXT_APP_API_URL: "http://localhost:5000/api",
// },
