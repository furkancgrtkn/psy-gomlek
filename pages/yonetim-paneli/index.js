import React, { useEffect, useState } from "react";
import Layout from "../../components/Layout/Layout";
import ProductPanel from "../../components/ProductPanel";
import Shippings from "../../components/Shippings";
import Users from "../../components/Users";
import styles from "../../styles/Pane.module.css";
import NoSsr from "../../components/utils/NoSsr";
import axios from "axios";
import Cookies from "universal-cookie";
import LoadingModule from "../../components/LoadingModule";

function admin() {
  const cookies = new Cookies();
  const [sectionNumb, setSection] = useState(1);
  const [users, setUsers] = useState([]);
  const [waitShipping, setWaitShipping] = useState([]);
  const [products, setProducts] = useState([]);
  const [loadingModule, setLoadingModule] = useState(false);

  useEffect(() => {
    setLoadingModule(true);

    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/auth/users`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setUsers(res.data.result);
        setTimeout(() => {
          setLoadingModule(false);
        }, 1000);
      })
      .catch((err) => console.log(err.response));
  }, []);

  const fetchWaitShips = () => {
    setLoadingModule(true);

    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/shipping/all_waiting`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setWaitShipping(res.data.data);
        setTimeout(() => {
          setLoadingModule(false);
        }, 1000);
      })
      .catch((err) => console.log(err.response));
  };

  const fetchProducts = () => {
    setLoadingModule(true);

    axios
      .get(
        process.env.REACT_APP_CLIENT_API_URL + `/products/all/products/get`,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setProducts(res.data.product);
        setTimeout(() => {
          setLoadingModule(false);
        }, 1000);
      })
      .catch((err) => console.log(err.response));
  };

  return (
    <NoSsr>
      <Layout>
        <div className={styles.panelMain}>
          <LoadingModule
            setLoadingModule={setLoadingModule}
            loadingModule={loadingModule}
          />
          <div className={styles.panelLinks}>
            <ul>
              <li>
                <button
                  style={
                    sectionNumb === 1
                      ? { color: "white", backgroundColor: "#333333" }
                      : null
                  }
                  onClick={() => setSection(1)}
                >
                  MÜŞTERİLER
                </button>
              </li>
              <li>
                <button
                  style={
                    sectionNumb === 2
                      ? { color: "white", backgroundColor: "#333333" }
                      : null
                  }
                  onClick={() => {
                    setSection(2);
                    fetchWaitShips();
                  }}
                >
                  SİPARİŞLER
                </button>
              </li>
              <li>
                <button
                  style={
                    sectionNumb === 3
                      ? { color: "white", backgroundColor: "#333333" }
                      : null
                  }
                  onClick={() => {
                    setSection(3);
                    fetchProducts();
                  }}
                >
                  ÜRÜNLER
                </button>
              </li>
            </ul>
          </div>
          {sectionNumb === 1 ? (
            <div className={styles.usersDiv}>
              <Users users={users} />
            </div>
          ) : null}

          {sectionNumb === 2 ? (
            <div className={styles.shipsDiv}>
              <Shippings
                shipping={waitShipping}
                fetchWaitShips={fetchWaitShips}
              />
            </div>
          ) : null}

          {sectionNumb === 3 ? (
            <div className={styles.shipsDiv}>
              <ProductPanel products={products} fetchProducts={fetchProducts} />
            </div>
          ) : null}
        </div>
      </Layout>
    </NoSsr>
  );
}
export default admin;

export async function getServerSideProps(context) {
  if (
    context.req.cookies.psyJWTUser
      ? JSON.parse(context.req.cookies.psyJWTUser).type === 0
      : true
  ) {
    return {
      redirect: {
        permanent: false,
        destination: "/",
      },
    };
  } else {
    return {
      props: { auth: false },
    };
  }
}
