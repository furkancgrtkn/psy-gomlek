import Link from "next/link";
import React, { useState } from "react";
import Layout from "../../components/Layout/Layout";
import styles from "../../styles/Login.module.css";
import axios from "axios";
import Head from "next/head";
import Cookies from "universal-cookie";
import { useRouter } from "next/router";

export default function giris() {
  const router = useRouter();
  const cookies = new Cookies();

  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [rememberMe, setRememberMe] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorMessageColor, setErrorMessageColor] = useState(true);

  const postLogin = async (e) => {
    e.preventDefault();
    let userObject = {
      email: email,
      password: password,
    };
    const maxAgeTime = rememberMe ? 86400 * 2 : null;
    await axios
      .post(process.env.REACT_APP_CLIENT_API_URL + `/auth/login`, userObject)
      .then((res) => {
        cookies.set("psyJWT", res.data.token, { maxAge: maxAgeTime });
        cookies.set("psyJWTUser", res.data.user[0], { maxAge: maxAgeTime });
        if (res.data.auth) {
          setErrorMessage("Giriş Başarılı");
          setErrorMessageColor(true);
          setTimeout(() => {
            router.replace("/hesabim");
          }, 500);
        }
      })
      .catch((err) => {
        setErrorMessage(err.response.data.message);
        setErrorMessageColor(false);
      });
  };

  return (
    <Layout>
      <Head>
        <title>PSY Gömlek | Giriş Yap</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.loginContainer}>
        <h4>Üye Girişi</h4>
        <form onSubmit={(e) => postLogin(e)} className={styles.loginForm}>
          {errorMessage ? (
            <span
              style={
                errorMessageColor
                  ? { backgroundColor: "#e0eede" }
                  : { backgroundColor: "#eedede" }
              }
            >
              {errorMessage}
            </span>
          ) : null}
          <div>
            <label>E Posta</label>
            <input
              required
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              placeholder="E Posta"
            ></input>
          </div>
          <div>
            <label>Şifre</label>
            <input
              onChange={(e) => setPassword(e.target.value)}
              required
              type="password"
              placeholder="Şifre"
            ></input>
          </div>
          <div className={styles.checkLoginRem}>
            <input
              onChange={(e) => setRememberMe(e.target.value)}
              type="checkbox"
              placeholder="Şifre"
            ></input>
            <p>Beni Hatırla</p>
          </div>
          <button type="submit">GİRİŞ YAP</button>
        </form>
        <div className={styles.goRegisterBtn}>
          <Link href="/uye-ol">
            <a>
              <button>ÜYE OL</button>
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  );
}

export async function getServerSideProps(context) {
  if (context.req.cookies.psyJWT && context.req.cookies.psyJWTUser) {
    return {
      redirect: {
        permanent: false,
        destination: "/hesabim",
      },
    };
  } else {
    return {
      props: { auth: false },
    };
  }
}
