import Head from "next/head";
import Layout from "../components/Layout/Layout";
import styles from "../styles/Home.module.css";
import HomeTopSwiper from "../components/HomeTopSwiper";
import HomeBanner from "../components/HomeBanner";
import HighlightedProduct from "../components/HighlightedProduct";
import { useState, useEffect } from "react";
export default function Home({ products }) {
  const [perView, setPerView] = useState(3);
  useEffect(() => {
    window.innerWidth > 1300 ? setPerView(3) : null;
    window.innerWidth < 1100 ? setPerView(2) : null;
    window.innerWidth < 750 ? setPerView(1) : null;
  }, []);
  return (
    <Layout>
      <Head>
        <title>PSY Gömlek | Anasayfa</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.container}></div>
      <HomeTopSwiper />
      <HomeBanner />
      <HighlightedProduct
        products={products}
        perView={perView}
        title={"ÖNE ÇIKANLAR"}
      />
    </Layout>
  );
}

export async function getServerSideProps(context) {
  const res = await fetch(
    process.env.NEXT_APP_API_URL + `/products/productwithpage/1`
  );
  const data = await res.json();
  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      products: data.data,
    }, // will be passed to the page component as props
  };
}
