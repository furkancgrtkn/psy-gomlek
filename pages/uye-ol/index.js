import React, { useState } from "react";
import Layout from "../../components/Layout/Layout";
import styles from "../../styles/RegisterPage.module.css";
import Link from "next/link";
import axios from "axios";
import { useRouter } from "next/router";
import Head from "next/head";

export default function uye() {
  const router = useRouter();

  const [name, setName] = useState(null);
  const [surName, setSurName] = useState(null);
  const [email, setEmail] = useState(null);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [password, setPassword] = useState(null);
  const [rePassword, setRePassword] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorMessageColor, setErrorMessageColor] = useState(true);

  const postRegister = async (e) => {
    e.preventDefault();
    const userObject = {
      name: name,
      surname: surName,
      email: email,
      password: password,
      phone_number: phoneNumber,
    };
    axios
      .post(process.env.REACT_APP_CLIENT_API_URL + `/auth/register`, userObject)
      .then((resp) => {
        setErrorMessage("Kayıt Başarılı");
        setErrorMessageColor(true);
        setTimeout(() => {
          router.replace("/giris-yap");
        }, 500);
      })
      .catch((err) => {
        setErrorMessage(err.response.data.message);
        setErrorMessageColor(false);
      });
  };
  return (
    <Layout>
      <Head>
        <title>PSY Gömlek | Üye Ol</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.registerContainer}>
        <h4>Üye Ol</h4>
        <form onSubmit={(e) => postRegister(e)} className={styles.registerForm}>
          {errorMessage ? (
            <span
              style={
                errorMessageColor
                  ? { backgroundColor: "#e0eede" }
                  : { backgroundColor: "#eedede" }
              }
            >
              {errorMessage}
            </span>
          ) : null}
          <div>
            <label>İsim</label>
            <input
              onChange={(e) => setName(e.target.value)}
              required
              type="text"
              placeholder="İsim"
            ></input>
          </div>
          <div>
            <label>Soy İsim</label>
            <input
              onChange={(e) => setSurName(e.target.value)}
              required
              type="text"
              placeholder="Soy İsim"
            ></input>
          </div>
          <div>
            <label>E Posta</label>
            <input
              onChange={(e) => setEmail(e.target.value)}
              required
              type="email"
              placeholder="E Posta"
            ></input>
          </div>
          <div>
            <label>Telefon Numarası</label>
            <input
              required
              onChange={(e) => setPhoneNumber(e.target.value)}
              type="text"
              maxLength="11"
              placeholder="Telefon Numarası"
            ></input>
          </div>
          <div>
            <label>Şifre</label>
            <input
              onChange={(e) => setPassword(e.target.value)}
              required
              type="password"
              placeholder="Şifre"
            ></input>
          </div>
          <div>
            <label>Şifrenizi Onaylayın</label>
            <input
              onChange={(e) => setRePassword(e.target.value)}
              required
              type="password"
              placeholder="Şifre"
            ></input>
          </div>
          <p>
            Üye Ol butonuna tıklayarak <b>Üyelik Sözleşmesi'ni</b> kabul etmiş
            olursunuz.
          </p>
          <button type="submit">ÜYE OL</button>
        </form>
        <div className={styles.goLoginBtn}>
          <Link href="/giris-yap">
            <a>
              <button>GİRİŞ YAP</button>
            </a>
          </Link>
        </div>
      </div>
    </Layout>
  );
}

export async function getServerSideProps(context) {
  if (context.req.cookies.psyJWT && context.req.cookies.psyJWTUser) {
    return {
      redirect: {
        permanent: false,
        destination: "/hesabim",
      },
    };
  } else {
    return {
      props: { auth: false },
    };
  }
}
