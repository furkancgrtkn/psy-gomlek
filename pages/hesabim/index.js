import Head from "next/head";
import Layout from "../../components/Layout/Layout";
import styles from "../../styles/MyAccountPage.module.css";
import MyAccountInfo from "../../components/MyAccountInfo";
import MyOrders from "../../components/MyOrders";
import { useEffect, useState } from "react";
import { VscAccount } from "react-icons/vsc";
import { FiLogOut, FiShoppingCart } from "react-icons/fi";
import { FiBell } from "react-icons/fi";
import Link from "next/link";
import Cookies from "universal-cookie";
import withAuth from "../../components/utils/withAuth";
import NoSsr from "../../components/utils/NoSsr";
import { useRouter } from "next/router";

function hesabim() {
  const router = useRouter();
  const cookies = new Cookies();
  const [userInfo, setUserInfo] = useState({});

  const [section, setSection] = useState("hesapBilgilerim");
  useEffect(() => {
    setUserInfo(cookies.get("psyJWTUser"));
  }, []);

  const onLogoutFunc = () => {
    cookies.remove("psyJWT");
    cookies.remove("psyJWTUser");
    router.replace("/");
  };

  return (
    <NoSsr>
      <Layout>
        <Head>
          <title>PSY Gömlek | Hesabım</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <div className={styles.myAccountPage}>
          <ul className={styles.accountMenuSide}>
            <li onClick={() => setSection("hesapBilgilerim")}>
              {section === "hesapBilgilerim" ? (
                <p className={styles.activeSection}>
                  <VscAccount />
                  Hesap Bilgilerim
                </p>
              ) : (
                <p>
                  <VscAccount />
                  Hesap Bilgilerim
                </p>
              )}
            </li>
            <li>
              <Link href="/sepetim">
                <a>
                  <p>
                    <FiShoppingCart />
                    Sepetim
                  </p>
                </a>
              </Link>
            </li>
            <li onClick={() => setSection("siparislerim")}>
              {section === "siparislerim" ? (
                <p className={styles.activeSection}>
                  <FiBell />
                  Siparişlerim
                </p>
              ) : (
                <p>
                  <FiBell />
                  Siparişlerim
                </p>
              )}
            </li>
            <li onClick={() => onLogoutFunc()}>
              <p>
                <FiLogOut />
                Çıkış Yap
              </p>
            </li>
          </ul>

          <div className={styles.accountRight}>
            {section === "hesapBilgilerim" ? (
              <MyAccountInfo userInfo={userInfo} />
            ) : null}
            {section === "siparislerim" ? <MyOrders /> : null}
          </div>
        </div>
      </Layout>
    </NoSsr>
  );
}

export default withAuth(hesabim);
