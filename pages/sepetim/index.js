import Head from "next/head";
import Layout from "../../components/Layout/Layout";
import styles from "../../styles/MyBasket.module.css";
import MyAccountInfo from "../../components/MyAccountInfo";
import LoadingModule from "../../components/LoadingModule";
import { useState, useEffect } from "react";
import { BsTrash } from "react-icons/bs";
import { IoIosArrowForward } from "react-icons/io";
import axios from "axios";
import Cookies from "universal-cookie";
import withAuth from "../../components/utils/withAuth";
import NoSsr from "../../components/utils/NoSsr";
import Link from "next/link";

function MyBasket() {
  const cookies = new Cookies();
  const [basket, setBasket] = useState([]);
  const [basketPrice, setBasketPrice] = useState(null);
  const [basketNoDiscPrice, setBasketNoDiscPrice] = useState(null);
  const [loadingModule, setLoadingModule] = useState(false);

  useEffect(() => {
    setLoadingModule(true);
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/basket`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setBasket(res.data.data);
        setBasketPrice(res.data.totalPrice);
        setBasketNoDiscPrice(res.data.totalPriceNoDisc);
        setLoadingModule(false);
      });
  }, []);

  const deleteItem = (id) => {
    let itemObject = {
      id: id,
    };
    setLoadingModule(true);
    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/basket/delete_item`,
        itemObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        axios
          .get(process.env.REACT_APP_CLIENT_API_URL + `/basket`, {
            headers: {
              Authorization: `Bearer ${cookies.get("psyJWT")}`,
            },
          })
          .then((res) => {
            setBasket(res.data.data);
            setBasketPrice(res.data.totalPrice);
            setBasketNoDiscPrice(res.data.totalPriceNoDisc);

            setLoadingModule(false);
          });
      });
  };

  const itemIncrease = (id) => {
    let itemObject = {
      id: id,
    };
    axios
      .put(
        process.env.REACT_APP_CLIENT_API_URL + `/basket/inc_count`,
        itemObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        axios
          .get(process.env.REACT_APP_CLIENT_API_URL + `/basket`, {
            headers: {
              Authorization: `Bearer ${cookies.get("psyJWT")}`,
            },
          })
          .then((res) => {
            setBasket(res.data.data);
            setBasketPrice(res.data.totalPrice);
            setBasketNoDiscPrice(res.data.totalPriceNoDisc);
          });
      })
      .catch((err) => {});
  };

  const itemDecrease = (id) => {
    let itemObject = {
      id: id,
    };
    axios
      .put(
        process.env.REACT_APP_CLIENT_API_URL + `/basket/dec_count`,
        itemObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        axios
          .get(process.env.REACT_APP_CLIENT_API_URL + `/basket`, {
            headers: {
              Authorization: `Bearer ${cookies.get("psyJWT")}`,
            },
          })
          .then((res) => {
            setBasket(res.data.data);
            setBasketPrice(res.data.totalPrice);
            setBasketNoDiscPrice(res.data.totalPriceNoDisc);
          });
      })
      .catch((err) => {});
  };

  return (
    <NoSsr>
      <Layout>
        <Head>
          <title>PSY Gömlek | Sepetim</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <div className={styles.myBasketPage}>
          <h1 className={styles.title}>Sepetim</h1>

          {basket.length === 0 ? (
            <div className={styles.emptyBasket}>
              <h2>Sepetin Boş</h2>
              <Link href="/gomlekler/1">
                <a>
                  <button>
                    ALIŞVERİŞE BAŞLA <IoIosArrowForward />{" "}
                  </button>
                </a>
              </Link>
            </div>
          ) : null}
          <div className={styles.basketPageContainer}>
            <div className={styles.basketProductContainer}>
              {basket.map((basketProduct, index) => (
                <div key={index} className={styles.basketProduct}>
                  <div className={styles.basketProductImage}>
                    <img src={basketProduct.images.split(",")[0]} />
                  </div>
                  <ul>
                    <li>
                      <b>ÜRÜN:</b> {basketProduct.name}
                    </li>
                    <li>
                      <b>ÜRÜN KODU:</b> {basketProduct.code}
                    </li>
                    <li>
                      <b>ÜRÜN RENGİ:</b> {basketProduct.color_name}
                    </li>
                    <li>
                      <b>ÜRÜN BEDENİ:</b> {basketProduct.size_product} Beden
                    </li>
                    <li>
                      <b>ÜRÜN ADEDİ:</b> {basketProduct.count} ADET
                    </li>
                    <li>
                      <b>ÜRÜNÜN FİYATI:</b> {basketProduct.discounted_price}TL
                    </li>
                  </ul>

                  <div className={styles.basketButtonsAndTotalPrice}>
                    <div className={styles.productCountContainer}>
                      <button
                        onClick={() =>
                          itemDecrease(basketProduct.basket_items_id)
                        }
                      >
                        -
                      </button>
                      <input
                        value={basketProduct.count}
                        type="number"
                        readOnly
                        min="1"
                      />
                      <button
                        onClick={() =>
                          itemIncrease(basketProduct.basket_items_id)
                        }
                      >
                        +
                      </button>
                    </div>

                    <h3>
                      <span>
                        {basketProduct.count * basketProduct.price} TL
                      </span>
                      <br />
                      {basketProduct.count * basketProduct.discounted_price} TL
                    </h3>
                    <BsTrash
                      onClick={() => deleteItem(basketProduct.basket_items_id)}
                    />
                  </div>
                </div>
              ))}
            </div>
            <div className={styles.basketResultContainer}>
              <div className={styles.basketDesc}>
                <h2>Sipariş Özeti</h2>
                <h4>
                  Ürün Toplamı <span> {basketNoDiscPrice} TL</span>
                </h4>
                <h4>
                  Kargo Toplamı <span> 9.99 TL</span>
                </h4>
                <h4>
                  İndirim Toplamı{" "}
                  <span> {basketNoDiscPrice - basketPrice} TL</span>
                </h4>
                <h3>{basketPrice + 10} TL</h3>
              </div>
              <Link href="/sepetim/ozet">
                <a>
                  <button>
                    SEPETİ ONAYLA <IoIosArrowForward />{" "}
                  </button>
                </a>
              </Link>
            </div>
          </div>

          <LoadingModule
            loadingModule={loadingModule}
            setLoadingModule={setLoadingModule}
          />
        </div>
      </Layout>
    </NoSsr>
  );
}
export default withAuth(MyBasket);
