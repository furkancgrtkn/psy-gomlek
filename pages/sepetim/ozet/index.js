import Head from "next/head";
import Layout from "../../../components/Layout/Layout";
import styles from "../../../styles/MyBasketSummary.module.css";
import LoadingModule from "../../../components/LoadingModule";
import { useState, useEffect } from "react";
import { IoIosArrowForward } from "react-icons/io";
import axios from "axios";
import Cookies from "universal-cookie";
import withAuth from "../../../components/utils/withAuth";
import NoSsr from "../../../components/utils/NoSsr";
import { useRouter } from "next/router";
import Link from "next/link";

function MyBasketSummary() {
  const router = useRouter();
  const cookies = new Cookies();
  const [basket, setBasket] = useState([]);
  const [basketPrice, setBasketPrice] = useState(null);
  const [basketNoDiscPrice, setBasketNoDiscPrice] = useState(null);
  const [addresses, setAddresses] = useState([]);
  const [address, setAddress] = useState("");
  const [addressIndex, setAddressIndex] = useState();
  const [loadingModule, setLoadingModule] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorMessageColor, setErrorMessageColor] = useState(true);

  useEffect(() => {
    setLoadingModule(true);
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/basket`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        res.data.data.length > 0 ? null : router.replace("/gomlekler");
        setBasket(res.data.data);
        setBasketPrice(res.data.totalPrice);
        setBasketNoDiscPrice(res.data.totalPriceNoDisc);
      })
      .then((res) => {
        setLoadingModule(false);
      });
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + `/address`, {
        headers: {
          Authorization: `Bearer ${cookies.get("psyJWT")}`,
        },
      })
      .then((res) => {
        setAddresses(res.data.data);
      });
  }, []);

  const handleClickAddress = (index, address) => {
    setAddress(address);
    setAddressIndex(index);
  };

  const submitOrder = () => {
    let orderObject = {
      basket: basket,
      address_info: address,
      total_price: basketPrice,
    };
    if (address) {
      setLoadingModule(true);
      axios
        .post(
          process.env.REACT_APP_CLIENT_API_URL + `/shipping/start`,
          orderObject,
          {
            headers: {
              Authorization: `Bearer ${cookies.get("psyJWT")}`,
            },
          }
        )
        .then((res) => {
          router.replace("/hesabim");
          // setLoadingModule(false);
        });
    } else {
      setErrorMessage("Lütfen Bir Adres Seçiniz");
      setErrorMessageColor(false);
    }
  };

  return (
    <NoSsr>
      <Layout>
        <Head>
          <title>PSY Gömlek | Özet</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <div className={styles.myBasketPage}>
          <h1 className={styles.title}>Sepetim</h1>
          <LoadingModule
            loadingModule={loadingModule}
            setLoadingModule={setLoadingModule}
          />
          <div className={styles.basketProductContainer}>
            {basket.map((basketProduct, index) => (
              <div key={index} className={styles.basketProduct}>
                <div className={styles.basketProductImage}>
                  <img src={basketProduct.images.split(",")[0]} />
                </div>
                <ul>
                  <li>
                    <b>ÜRÜN:</b> {basketProduct.name}
                  </li>
                  <li>
                    <b>ÜRÜN KODU:</b> {basketProduct.code}
                  </li>
                  <li>
                    <b>ÜRÜN RENGİ:</b> {basketProduct.color_name}
                  </li>
                  <li>
                    <b>ÜRÜN BEDENİ:</b> {basketProduct.size_product} Beden
                  </li>
                  <li>
                    <b>ÜRÜN ADEDİ:</b> {basketProduct.count} ADET
                  </li>
                  <li>
                    <b>ÜRÜNÜN FİYATI:</b> {basketProduct.price}TL
                  </li>
                </ul>
              </div>
            ))}
          </div>
          <h1 className={styles.title}>Adres Seç</h1>
          <div className={styles.adddressContainer}>
            {addresses.map((address, index) => (
              <div
                onClick={() => handleClickAddress(index, address)}
                key={index}
                className={styles.addressWrapper}
              >
                <h3
                  style={
                    addressIndex === index
                      ? { backgroundColor: "#7dbd5d", color: "white" }
                      : null
                  }
                >
                  {address.title}
                </h3>
                <div className={styles.addressTextContainer}>
                  <span>{address.phone_number}</span>
                  <span>{address.detail}</span>
                  <span>{`${address.city_name}/${address.district_name}`}</span>
                </div>
              </div>
            ))}

            {addresses.length > 0 ? null : (
              <Link href="/hesabim">
                <a>
                  <button className={styles.addAddressLink}>
                    Adres Eklemek İçin Tıkla <IoIosArrowForward />{" "}
                  </button>
                </a>
              </Link>
            )}
          </div>
          <h1 className={styles.title}>Sipariş Özeti ve Onay</h1>
          <div className={styles.basketResultContainer}>
            <div className={styles.basketDesc}>
              <h2>Sipariş Özeti</h2>
              <h4>
                Ürün Toplamı <span> {basketNoDiscPrice} TL</span>
              </h4>
              <h4>
                Kargo Toplamı <span> 9.99 TL</span>
              </h4>
              <h4>
                İndirim Toplamı{" "}
                <span> {basketNoDiscPrice - basketPrice} TL</span>
              </h4>
              <h3>{basketPrice + 10} TL</h3>
            </div>
            {errorMessage ? (
              <p
                className={styles.errorMessage}
                style={
                  errorMessageColor
                    ? { backgroundColor: "#e0eede" }
                    : { backgroundColor: "#eedede" }
                }
              >
                {errorMessage}
              </p>
            ) : null}
            <button onClick={submitOrder}>
              SEPETİ ONAYLA <IoIosArrowForward />{" "}
            </button>
          </div>
        </div>
      </Layout>
    </NoSsr>
  );
}
export default withAuth(MyBasketSummary);
