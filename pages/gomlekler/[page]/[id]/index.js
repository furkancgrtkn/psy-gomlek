import Head from "next/head";
import Layout from "../../../../components/Layout/Layout";
import styles from "../../../../styles/ProductPage.module.css";
import { Form, Radio, Button } from "semantic-ui-react";
import { useState, useEffect } from "react";
import Link from "next/link";
import HighlightedProduct from "../../../../components/HighlightedProduct";
import Cookies from "universal-cookie";
import axios from "axios";
import { useRouter } from "next/router";
import { SwiperSlide, Swiper } from "swiper/react";
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Thumbs,
} from "swiper";
import "swiper/swiper-bundle.css";
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Thumbs]);

export default function gomlek({ product, colors, recommendedProducts }) {
  const cookies = new Cookies();
  const router = useRouter();
  const [size, setSize] = useState();
  const handleSizeChange = (e, { value }) => setSize(value);
  const [accordion1, setAccordion1] = useState(false);
  const [accordion2, setAccordion2] = useState(false);
  const [accordion3, setAccordion3] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorMessageColor, setErrorMessageColor] = useState(true);

  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const [perView, setPerView] = useState(4);
  const [perViewRecommended, setPerViewRecommended] = useState(3);

  const [addButton, setAddButton] = useState(false);

  useEffect(() => {
    return () => {
      window.innerWidth < 711 ? setPerView(3) : null;
      window.innerWidth < 491 ? setPerView(2) : null;
    };
  });

  const addItem = () => {
    let itemObject = {
      product_id: product.product_id,
      count: 1,
      size: size,
    };
    setAddButton(true);

    axios
      .post(
        process.env.REACT_APP_CLIENT_API_URL + `/basket/add_item`,
        itemObject,
        {
          headers: {
            Authorization: `Bearer ${cookies.get("psyJWT")}`,
          },
        }
      )
      .then((res) => {
        setErrorMessage(res.data.message);
        setErrorMessageColor(true);
        setTimeout(() => {
          setAddButton(false);
          setErrorMessage(null);
        }, 1000);
      })
      .catch((err) => {
        setErrorMessage(err.response.data.message);
        setErrorMessageColor(false);
        setAddButton(false);
        setSize(null);
      });
  };

  useEffect(() => {
    window.innerWidth > 1300 ? setPerViewRecommended(3) : null;
    window.innerWidth < 1100 ? setPerViewRecommended(2) : null;
    window.innerWidth < 750 ? setPerViewRecommended(1) : null;
  }, []);

  return (
    <Layout>
      <Head>
        <title>{product.name}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.productPage}>
        <div className={styles.imgSide}>
          <div className={styles.mainSliderContainerProduct}>
            <Swiper
              navigation
              className="productDetailMainSilder"
              thumbs={{ swiper: thumbsSwiper }}
            >
              {product.images.split(",").map((img, index) => (
                <SwiperSlide key={index}>
                  <div className={styles.imageWrap}>
                    <img
                      quality={50}
                      // src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615460430/banner_wwe2ns.jpg"
                      src={img}
                      alt="homepagebottomsliderImg"
                    />
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
          <div className={styles.thumbsSliderContainerProduct}>
            <Swiper
              onSwiper={setThumbsSwiper}
              watchSlidesVisibility
              watchSlidesProgress
              slidesPerView={perView}
              className="productDetailThumbs"
            >
              {product.images.split(",").map((img, index) => (
                <SwiperSlide key={index}>
                  <div className={styles.thumbsImageContainer}>
                    <img quality={50} src={img} alt="homepagebottomsliderImg" />
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </div>
        <Form
          onSubmit={
            cookies.get("psyJWT")
              ? (e) => addItem(e)
              : (e) => router.replace("/giris-yap")
          }
        >
          <div className={styles.descSide}>
            <h1>{product.name}</h1>
            <h4>
              <span>{product.price} TL</span>
              <br />
              {product.discounted_price} TL
            </h4>

            <p className={styles.colorChoicesTitle}>Renk Seçenekleri</p>
            <div className={styles.colorChoices}>
              {colors.map((color, index) => (
                <li key={index}>
                  <Link href={`/gomlekler/urun-detayi/${color.product_id}`}>
                    <a>
                      <div className={styles.colorChoiceImg}>
                        <img
                          src={color.images.split(",")[0]}
                          alt={color.name}
                        />
                      </div>
                    </a>
                  </Link>
                  <h4>{color.color_name}</h4>
                </li>
              ))}
            </div>

            <p className={styles.colorChoicesTitle}>Beden Seçenekleri</p>
            <div className={styles.sizeChoices}>
              {product.size.split(",").map((sizeChoice, index) => (
                <Form.Field required key={index}>
                  <Radio
                    label={sizeChoice}
                    name="radioGroup"
                    value={sizeChoice}
                    checked={size === sizeChoice}
                    onChange={handleSizeChange}
                    className="sizeRadio"
                    required
                  />
                </Form.Field>
              ))}
            </div>
          </div>
          {errorMessage ? (
            <p
              className={styles.errorMessage}
              style={
                errorMessageColor
                  ? { backgroundColor: "#e0eede" }
                  : { backgroundColor: "#eedede" }
              }
            >
              {errorMessage}
            </p>
          ) : null}
          <Button
            style={
              addButton
                ? { backgroundColor: "green", color: "white", width: "100%" }
                : { width: "100%" }
            }
            disabled={addButton}
            type="submit"
          >
            {addButton ? "SEPETE EKLENİYOR" : "SEPETE EKLE"}
          </Button>

          <div className={styles.accordionBox}>
            <p onClick={() => setAccordion1(!accordion1)}>
              Detaylar <span> {accordion1 ? "-" : "+"}</span>
            </p>
            <ul className={accordion1 ? styles.acoordionBoxOpen : null}>
              <li>{product.detail}</li>
              <li>{product.color_name}</li>
              <li>{product.fit === 1 ? "Slim Fit" : "Klasik"}</li>
              <li>{product.sleeve === 0 ? "Kısa Kollu" : "Uzun Kollu"}</li>
              <li>{product.code}</li>
            </ul>
          </div>
          <div className={styles.accordionBox}>
            <p onClick={() => setAccordion2(!accordion2)}>
              Teslimat <span> {accordion2 ? "-" : "+"}</span>
            </p>
            <ul className={accordion2 ? styles.acoordionBoxOpen : null}>
              <li>
                Siparişleriniz ortalama 3-4 iş günü içerisinde kargoya teslim
                edilir. Siparişinizin durumunu "Siparişlerim" bölümünden takip
                edebilirsiniz.
              </li>
            </ul>
          </div>
          <div className={styles.accordionBox}>
            <p onClick={() => setAccordion3(!accordion3)}>
              İptal & İade <span> {accordion3 ? "-" : "+"}</span>
            </p>
            <ul className={accordion3 ? styles.acoordionBoxOpen : null}>
              <li>
                İnternet sitemizden satın aldığınız kullanılmamış veya tadilat
                yapılmamış ürününüzü, teslim tarihinden itibaren 30 (otuz) gün
                içerisinde faturanız ile iade edebilirsiniz. Detaylı bilgi için
                iletişime geçiniz.
              </li>
            </ul>
          </div>
        </Form>
      </div>
      <HighlightedProduct
        products={recommendedProducts}
        perView={perViewRecommended}
        title={"ÖNERİLENLER"}
      />
    </Layout>
  );
}

export async function getServerSideProps(context) {
  const id = context.query.id;
  const res = await fetch(
    process.env.NEXT_APP_API_URL + `/products/detail/${id}`
  );
  const data = await res.json();

  const res2 = await fetch(
    process.env.NEXT_APP_API_URL + `/products/productwithpage/1`
  );
  const dataRecommended = await res2.json();

  if (!data || !dataRecommended) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      product: data.product[0],
      colors: data.colors,
      recommendedProducts: dataRecommended.data,
    },
  };
}
