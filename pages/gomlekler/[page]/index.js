import Head from "next/head";
import Layout from "../../../components/Layout/Layout";
import styles from "../../../styles/ProductsPage.module.css";
import ProductCard from "../../../components/ProductCard";
import { Dropdown, Pagination } from "semantic-ui-react";
import { useState, useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/router";
export default function gomlekler({ productsSSR, currentPage, maxPage }) {
  const router = useRouter();
  const fitOptions = [
    { text: "Slim Fit", value: "slim-fit" },
    { text: "Klasik", value: "klasik" },
  ];

  const sleeveOptions = [
    { text: "Uzun Kollu", value: "uzun-kollu" },
    { text: "Kısa Kollu", value: "kisa-kollu" },
  ];

  const orderOptions = [
    { text: "Fiyat Artan", value: "artan" },
    { text: "Fiyat Azalan", value: "azalan" },
  ];

  // const [value, setValue] = useState("");
  const [fit, setFit] = useState("tum");
  const [allColors, setAllColors] = useState([]);
  const [sleeve, setSleeve] = useState("urunler");
  const [colorz, setColorz] = useState("renkler");
  const [orderBy, setOrderBy] = useState("normal");
  const [products, setProducts] = useState(productsSSR);
  const [loadingModule, setLoadingModule] = useState(false);
  const [currentPage2, setCurrentPage2] = useState(currentPage);
  const [maxPage2, setMaxPage2] = useState(maxPage);
  const handleChangeFit = (e, { value }) => setFit(value);
  const handleChangeSleeve = (e, { value }) => setSleeve(value);
  const handleChangeColorz = (e, { value }) => setColorz(value);
  const handleChangeOrder = (e, { value }) => setOrderBy(value);

  const handleChangeFilter = () => {
    axios
      .get(
        process.env.REACT_APP_CLIENT_API_URL +
          `/products/filter/${currentPage}/${fit}/${sleeve}/${colorz}/${orderBy}`
      )
      .then((res) =>
        // setProducts(res.data.data)
        {
          setProducts(res.data.data);
          setMaxPage2(res.data.max_page);
          setTimeout(() => {
            setLoadingModule(false);
          }, 1000);
        }
      );
    axios
      .get(process.env.REACT_APP_CLIENT_API_URL + "/products/get_colors")
      .then((resp) => {
        setAllColors(resp.data.data);
      });
  };

  useEffect(() => {
    setLoadingModule(true);
    handleChangeFilter();
  }, [currentPage]);

  useEffect(() => {
    setLoadingModule(true);
    handleChangeFilter();
    handlePaginationChange(1, { activePage: 1 });
  }, [fit, sleeve, colorz, orderBy]);

  const handlePaginationChange = (e, { activePage }) => {
    router.push(`/gomlekler/${activePage}`);
  };
  return (
    <Layout>
      <Head>
        <title>PSY Gömlek | Gömlekler</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={styles.pageContainer}>
        <div className={styles.imageWrap}>
          <img
            src="https://res.cloudinary.com/dhy7yh4aa/image/upload/v1615460430/banner_wwe2ns.jpg"
            alt="Gömlekler"
          />
        </div>
        <div className={styles.filterAndSort}>
          <div className={styles.filtersContainer}>
            <Dropdown
              className="fitDropdown"
              onChange={handleChangeFit}
              options={fitOptions}
              placeholder="Kalıp Seçin"
              selection
              value={fit}
            />
            <Dropdown
              className="fitDropdown"
              onChange={handleChangeSleeve}
              options={sleeveOptions}
              placeholder="Kol Seçin"
              selection
              value={sleeve}
            />
            <Dropdown
              className="fitDropdown"
              onChange={handleChangeColorz}
              options={allColors}
              placeholder="Renk Seçin"
              selection
              value={colorz}
            />
          </div>
          <Dropdown
            className="sortDropdown"
            onChange={handleChangeOrder}
            options={orderOptions}
            value={orderBy}
            placeholder="Sıralama"
            selection
            // value={value}
          />
        </div>

        <div className={styles.productsContainer}>
          {loadingModule ? (
            <div style={{ minHeight: "400px" }} className="loadingModal">
              <div className="lds-default">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
              </div>
              <p>Lütfen Bekleyiniz..</p>
            </div>
          ) : (
            products.map((product, index) => (
              <ProductCard product={product} key={index} />
            ))
          )}
          {loadingModule ? null : products.length > 0 ? null : (
            <h4 className={styles.notFoundProducts}>
              Bu Kriterlerde Ürün Bulunamadı
            </h4>
          )}
        </div>
        <div className={styles.productsPagination}>
          <Pagination
            boundaryRange={1}
            siblingRange={1}
            ellipsisItem={"..."}
            activePage={currentPage}
            onPageChange={handlePaginationChange}
            totalPages={maxPage2}
            firstItem={false}
            lastItem={false}
            // prevItem={true}
            // nextItem={true}
          />
        </div>
      </div>
    </Layout>
  );
}

export async function getServerSideProps(context) {
  const res = await fetch(
    process.env.NEXT_APP_API_URL +
      `/products/filter/${context.query.page}/tum/urunler/renkler/normal`
  );
  const data = await res.json();
  if (!data) {
    return {
      notFound: true,
    };
  }

  if (data.data === undefined || data.data.length === 0) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      productsSSR: data.data,
      currentPage: data.page_number,
      maxPage: data.max_page,
    },
  };
}
